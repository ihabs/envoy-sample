package com.env.example.service1.controller;

import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.env.example.service1.model.Resp;

@RestController
public class Service1 {

	@Autowired
	private Environment environment;
	
	private static AtomicInteger requestsCount = new AtomicInteger();
	
	@RequestMapping("/service1")
	public Resp print() {
		return new Resp("service 1", environment.getProperty("local.server.port"), String.valueOf(requestsCount.incrementAndGet()) ) ;
	}
	
	
}
