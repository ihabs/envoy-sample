package com.env.example.service1.model;
public class Resp {
	private String service;
	private String port;
	private String requestsCount;
	public Resp(String service, String port, String count) {
		super();
		this.service = service;
		this.port = port;
		this.requestsCount = count;
	}
	public String getService() {
		return service;
	}
	public String getPort() {
		return port;
	}
	public String getRequestsCount() {
		return requestsCount;
	}
}
