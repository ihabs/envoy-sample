docker stop envoy-discovery
docker rm envoy-discovery
docker build -t envoy-discovery:latest .
docker run -d --name envoy-discovery -p 9200:9200 envoy-discovery:latest