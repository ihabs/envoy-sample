docker stop envoy
docker rm envoy
docker build -t envoy:latest .
docker run -d --name envoy -p 9901:9901 -p 10000:10000 envoy:latest