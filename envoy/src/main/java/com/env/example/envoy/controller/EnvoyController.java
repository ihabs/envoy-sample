package com.env.example.envoy.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EnvoyController {

	@RequestMapping("/envoy")
	public String print() {
		return "envoy";

	}
	
	
}
